from data_base.base_model import Chat
from sqlalchemy.ext.asyncio import AsyncSession
from sqlalchemy import select


async def get_active_chat(session: AsyncSession):
    """
    Функция получения активных чатов для рассылки
    """
    query = (
        select(Chat)
        .filter(Chat.status == True)
    )
    chat = await session.execute(query)
    return chat.scalars().all()
