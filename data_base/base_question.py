from data_base.base_model import Question
from sqlalchemy.ext.asyncio import AsyncSession
from sqlalchemy import select, Date, func, update, delete
from datetime import datetime, timedelta
from aiogram.types import Message


async def add_question(session: AsyncSession, user_id: int,
                       text_question: str | None, type_question: str | None,
                       file_question: str | None):
    obj = Question(
        user_id=user_id,
        text_question=text_question,
        type_question=type_question,
        file_question=file_question,
        status_ask=False,
        status_send=False,
    )
    session.add(obj)
    await session.commit()
    await session.refresh(obj)
    return obj.id_question


async def update_question_question(session: AsyncSession, id_question,
                                   question):
    query = (
        update(Question).filter(Question.id_question == id_question).
        values(text_question=question)
    )
    await session.execute(query)
    await session.commit()


async def update_question_question_file(session: AsyncSession, id_question,
                                        type_question: str, file_question: str):
    query = (
        update(Question).filter(Question.id_question == id_question).
        values(file_question=file_question, type_question=type_question)
    )
    await session.execute(query)
    await session.commit()


async def update_question_plan(session: AsyncSession, id_question,
                               plan_id):
    query = (
        update(Question).filter(Question.id_question == id_question).
        values(plan_id=plan_id)
    )
    await session.execute(query)
    await session.commit()


async def del_question(session, id_question):
    query = (
        delete(Question).filter(Question.id_question == id_question)
    )
    await session.execute(query)
    await session.commit()


async def get_count_question_user(session: AsyncSession, user_id):
    """
    Функция получения количества вопросов пользователя
    """
    query = (
        select(func.count(Question.id_question))
        .filter(Question.user_id == user_id)
    )
    users = await session.execute(query)
    count = users.scalar()
    return count


async def get_question_by_id(session: AsyncSession, id_question):
    """
    Функция получения количества вопросов пользователя
    """
    query = (
        select(Question)
        .filter(Question.id_question == id_question)
    )
    question = await session.execute(query)
    return question.scalar()


async def get_question_by_user_id(session: AsyncSession, user_id: int):
    query = select(Question).filter(Question.user_id == user_id)
    asks_user = await session.execute(query)
    return asks_user.scalars().all()


async def update_update_question_status_send(session: AsyncSession, id_question):
    query = (
        update(Question).filter(Question.id_question == id_question).
        values(status_send=True)
    )
    await session.execute(query)
    await session.commit()


async def get_questions_active(session: AsyncSession):
    query = select(Question).filter(Question.status_ask == False)
    asks_user = await session.execute(query)
    return asks_user.scalars().all()


async def update_status_ask_and_ask(session: AsyncSession, id_question,
                                    aks, file: str | None,
                                    type_file: str | None):
    query = (
        update(Question).filter(Question.id_question == id_question).
        values(status_ask=True, text_ask=aks, file_ask=file, type_ask=type_file)
    )
    await session.execute(query)
    await session.commit()
