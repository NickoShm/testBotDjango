from data_base.base_model import Message, Button, Plans, Users, Wallets
from sqlalchemy.ext.asyncio import AsyncSession
from sqlalchemy import select, Date, func
from datetime import datetime, timedelta
from aiogram.types import Message, CallbackQuery


async def add_new_user(session: AsyncSession, data: Message):

    user = await session.get(Users, data.from_user.id)
    if user is None:
        obj = Users(
            user_id=data.from_user.id,
            first_name=data.from_user.first_name,
            last_name=data.from_user.last_name,
            username=data.from_user.username,
            is_admin=False,
        )
        obj_w = Wallets(
            user_id=data.from_user.id,
            cash=0,
        )
        session.add(obj)
        session.add(obj_w)
        await session.commit()


async def add_new_user_callback(session: AsyncSession, callback: CallbackQuery):

    user = await session.get(Users, callback.from_user.id)
    if user is None:
        obj = Users(
            user_id=callback.from_user.id,
            first_name=callback.from_user.first_name,
            last_name=callback.from_user.last_name,
            username=callback.from_user.username,
            is_admin=False,
        )
        obj_w = Wallets(
            user_id=callback.from_user.id,
            cash=0,
        )
        session.add(obj)
        session.add(obj_w)
        await session.commit()

