from data_base.base_model import Message, Button, Plans, ButtonPay
from sqlalchemy.ext.asyncio import AsyncSession
from sqlalchemy import select, Date, func
from datetime import datetime, timedelta


async def get_message_by_id(session: AsyncSession, id_message: int):
    message = await session.get(Message, id_message)
    return message.text


async def get_button_by_id(session: AsyncSession, id_message: int):
    button = await session.get(Button, id_message)
    return button.text


async def get_plans(session: AsyncSession):
    query = select(Plans)
    plans = await session.execute(query)
    return plans.scalars().all()


async def get_method_pay(session: AsyncSession):
    query = select(ButtonPay)
    method_pay = await session.execute(query)
    return method_pay.scalars().all()

