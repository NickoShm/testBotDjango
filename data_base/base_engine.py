from sqlalchemy.ext.asyncio import create_async_engine, async_sessionmaker
# from config import app_config
from data_base.base_model import Base
import os


# user = app_config.postgres.user
# password = app_config.postgres.password.get_secret_value()
# host = app_config.postgres.host
# port = app_config.postgres.port
# name_db = app_config.postgres.db


engine = create_async_engine(f'postgresql+asyncpg://{os.getenv("POSTGRES_USER")}:{os.getenv("POSTGRES_PASSWORD")}'
                             f'@{os.getenv("POSTGRES_HOST")}:{os.getenv("POSTGRES_PORT")}'
                             f'/{os.getenv("POSTGRES_DB")}')

# engine = create_async_engine(f'postgresql+asyncpg://{user}:{password}@{host}:{port}/{name_db}')


session_maker = async_sessionmaker(engine)


async def create_table():
    async with engine.begin() as conn:
        await conn.run_sync(Base.metadata.create_all)
