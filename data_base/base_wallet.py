from data_base.base_model import Message, Button, Plans, Users, Wallets
from sqlalchemy.ext.asyncio import AsyncSession
from sqlalchemy import select, Date, func, update
from datetime import datetime, timedelta
from aiogram.types import Message


async def get_wallet_by_user_id(session: AsyncSession, user_id: int):
    wallet = await session.get(Wallets, user_id)
    return round(wallet.cash, 2)


async def update_wallet_user(session: AsyncSession, user_id: int,
                             wallet):
    query = (
        update(Wallets).filter(Wallets.user_id == user_id).
        values(cash=wallet)
    )
    await session.execute(query)
    await session.commit()


