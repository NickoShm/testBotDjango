from data_base.base_model import Question, Plans
from sqlalchemy.ext.asyncio import AsyncSession
from sqlalchemy import select, Date, func, update, delete
from datetime import datetime, timedelta
from aiogram.types import Message


async def get_plan_by_id(session: AsyncSession, id_plan):
    """
    Функция получения количества вопросов пользователя
    """
    query = (
        select(Plans)
        .filter(Plans.id_plan == id_plan)
    )
    plan = await session.execute(query)
    return plan.scalar()
