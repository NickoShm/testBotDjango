from data_base.base_model import Transactions
from sqlalchemy.ext.asyncio import AsyncSession
from sqlalchemy import select, Date, func
from datetime import datetime, timedelta
from aiogram.types import Message


async def add_transaction(session: AsyncSession, user_id: int, cash: float):
    obj = Transactions(
        user_id=user_id,
        cash=cash,
    )
    session.add(obj)
    await session.commit()
    await session.refresh(obj)
    return obj.id_transaction
