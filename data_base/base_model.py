from sqlalchemy.orm import DeclarativeBase, Mapped, mapped_column
from sqlalchemy import (DateTime, func, String, Text, ForeignKey,
                        BigInteger, Integer, Float, Boolean, text)


class Base(DeclarativeBase):
    pass


class Message(Base):
    __tablename__ = 'Components_message'

    id_message: Mapped[int] = mapped_column(Integer, primary_key=True)
    text: Mapped[str]


class Button(Base):
    __tablename__ = 'Components_button'

    id_button: Mapped[int] = mapped_column(Integer, primary_key=True)
    text: Mapped[str]


class ButtonPay(Base):
    __tablename__ = 'Components_pay'

    id_button: Mapped[int] = mapped_column(Integer, primary_key=True)
    text: Mapped[str]
    callback: Mapped[str]


class Plans(Base):
    __tablename__ = 'Components_plan'

    id_plan: Mapped[int] = mapped_column(Integer, primary_key=True)
    name: Mapped[str] = mapped_column(String(50))
    price: Mapped[float] = mapped_column(Float(asdecimal=True), nullable=False)


class Users(Base):
    __tablename__ = 'Users_user'

    user_id: Mapped[int] = mapped_column(BigInteger, primary_key=True)
    first_name: Mapped[str] = mapped_column(String(50), nullable=True)
    last_name: Mapped[str] = mapped_column(String(50), nullable=True)
    username: Mapped[str] = mapped_column(String(50), nullable=True)
    created_at: Mapped[DateTime] = mapped_column(DateTime(timezone=True), default=text("TIMEZONE('Europe/Moscow', now())"))
    updated_at: Mapped[DateTime] = mapped_column(DateTime, default=text("TIMEZONE('Europe/Moscow', now())"),
                                                 onupdate=text("TIMEZONE('Europe/Moscow', now())"))
    is_admin: Mapped[bool] = mapped_column(Boolean)


class Wallets(Base):
    __tablename__ = 'Users_wallet'

    user_id: Mapped[int] = mapped_column(ForeignKey(
        'Users_user.user_id', ondelete='CASCADE'), primary_key=True)
    cash: Mapped[float] = mapped_column(Float(asdecimal=True), nullable=False)
    created_at: Mapped[DateTime] = mapped_column(DateTime, default=text("TIMEZONE('Europe/Moscow', now())"))
    updated_at: Mapped[DateTime] = mapped_column(DateTime, default=text("TIMEZONE('Europe/Moscow', now())"),
                                                 onupdate=text("TIMEZONE('Europe/Moscow', now())"))


class Transactions(Base):
    __tablename__ = 'Users_transaction'

    id_transaction: Mapped[int] = mapped_column(primary_key=True, autoincrement=True)
    user_id: Mapped[int] = mapped_column(ForeignKey(
        'Users_user.user_id', ondelete='CASCADE'), primary_key=True)
    cash: Mapped[float] = mapped_column(Float(asdecimal=True), nullable=False)
    created_at: Mapped[DateTime] = mapped_column(DateTime, default=text("TIMEZONE('Europe/Moscow', now())"))


class Question(Base):
    __tablename__ = 'Users_question'

    id_question: Mapped[int] = mapped_column(primary_key=True, autoincrement=True)
    user_id: Mapped[int] = mapped_column(ForeignKey(
        'Users_user.user_id', ondelete='CASCADE'), primary_key=True)
    text_question: Mapped[str] = mapped_column(Text, nullable=True)
    file_question: Mapped[str] = mapped_column(String(100), nullable=True)
    type_question: Mapped[str] = mapped_column(String(10), nullable=True)
    text_ask: Mapped[str] = mapped_column(Text, nullable=True)
    file_ask: Mapped[str] = mapped_column(String(100), nullable=True)
    type_ask: Mapped[str] = mapped_column(String(10), nullable=True)
    plan_id: Mapped[int] = mapped_column(ForeignKey(
        'Components_plan.id_plan', ondelete='CASCADE'), primary_key=True,
        nullable=True)
    status_send: Mapped[bool] = mapped_column(Boolean)
    status_ask: Mapped[bool] = mapped_column(Boolean)
    created_at: Mapped[DateTime] = mapped_column(DateTime(timezone=True), default=text("TIMEZONE('Europe/Moscow', now())"))
    updated_at: Mapped[DateTime] = mapped_column(DateTime(timezone=True), default=text("TIMEZONE('Europe/Moscow', now())"),
                                                 onupdate=text("TIMEZONE('Europe/Moscow', now())"))


class Chat(Base):
    __tablename__ = 'Components_chat'

    id_chat: Mapped[int] = mapped_column(Integer, primary_key=True)
    name: Mapped[str] = mapped_column(String(50))
    url: Mapped[str] = mapped_column(String(50))
    status: Mapped[bool] = mapped_column(Boolean)
