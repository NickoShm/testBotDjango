from django.contrib import admin
from .models import Message, Button, Plan, Chat, Pay


class MessageAdmin(admin.ModelAdmin):
    list_display = ['id_message',
                    'text',]
    list_display_links = ['text']


class ButtonAdmin(admin.ModelAdmin):
    list_display = ['id_button',
                    'text',]
    list_display_links = ['text']


class PayAdmin(admin.ModelAdmin):
    list_display = ['id_button',
                    'text',
                    'callback']
    list_display_links = ['text', 'callback']


class PlanAdmin(admin.ModelAdmin):
    list_display = ['id_plan', 'name', 'price',]
    list_display_links = ['name', 'price',]


class ChatAdmin(admin.ModelAdmin):
    list_display = ['id_chat',
                    'name',
                    'url',
                    'status', ]
    list_display_links = ['name',
                          'url',
                          'status', ]


admin.site.register(Message, MessageAdmin)
admin.site.register(Button, ButtonAdmin)
admin.site.register(Plan, PlanAdmin)
admin.site.register(Chat, ChatAdmin)
admin.site.register(Pay, PayAdmin)
