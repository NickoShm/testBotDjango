from django.db import models


class Message(models.Model):
    id_message = models.AutoField(primary_key=True,
                                  verbose_name='id')
    text = models.TextField(verbose_name='Текст сообщения')

    class Meta:
        verbose_name = 'Сообщение'
        verbose_name_plural = 'Сообщения'

    def __str__(self):
        return f'{self.text}'


class Button(models.Model):
    id_button = models.AutoField(primary_key=True,
                                 verbose_name='id')
    text = models.CharField(verbose_name='Текст кнопки',
                            max_length=50)

    class Meta:
        verbose_name = 'Кнопка'
        verbose_name_plural = 'Кнопки'

    def __str__(self):
        return f'{self.text}'


class Plan(models.Model):
    id_plan = models.AutoField(primary_key=True,
                               verbose_name='id')
    name = models.CharField(verbose_name='Название тарифа', max_length=50)
    price = models.PositiveIntegerField(verbose_name='Стоимость')

    class Meta:
        verbose_name = 'План'
        verbose_name_plural = 'Планы'

    def __str__(self):
        return f'{self.name} {self.price}'


class Chat(models.Model):
    id_chat = models.AutoField(primary_key=True,
                               verbose_name='id')
    name = models.CharField(verbose_name='Название группы (канала)', max_length=50)
    url = models.CharField(verbose_name='url', max_length=50)
    status = models.BooleanField(default=True, verbose_name='Статус')

    class Meta:
        verbose_name = 'Чат'
        verbose_name_plural = 'Чаты'

    def __str__(self):
        return f'{self.name} {self.url}'


class Pay(models.Model):
    id_button = models.AutoField(primary_key=True,
                                 verbose_name='id')
    text = models.CharField(verbose_name='Текст кнопки',
                            max_length=50)

    callback = models.CharField(verbose_name='Способ оплаты',
                                max_length=50)

    class Meta:
        verbose_name = 'Кнопка оплаты'
        verbose_name_plural = 'Кнопки оплаты'

    def __str__(self):
        return f'{self.text}'
