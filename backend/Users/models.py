from django.db import models


class User(models.Model):
    user_id = models.PositiveBigIntegerField(primary_key=True,
                                             verbose_name='Telegram ID')
    first_name = models.CharField(max_length=256, null=True, blank=True,
                                  verbose_name='Имя Tg')
    last_name = models.CharField(max_length=256, null=True, blank=True,
                                 verbose_name='Фамилия Tg')
    username = models.CharField(max_length=100, null=True, blank=True,
                                verbose_name='Username')
    created_at = models.DateTimeField(auto_now_add=True, db_index=True,
                                      verbose_name='Создан')
    updated_at = models.DateTimeField(auto_now=True,
                                      verbose_name='Обновлен')
    is_admin = models.BooleanField(default=False,
                                   verbose_name='Права админа')

    class Meta:
        ordering = ('-created_at',)
        verbose_name = 'Пользователь'
        verbose_name_plural = 'Пользователи'

    def __str__(self):
        return f'{self.first_name}'


class Wallet(models.Model):
    user = models.ForeignKey(User,
                             on_delete=models.SET_NULL,
                             null=True,
                             related_name='wallet',
                             verbose_name='Пользователь')
    cash = models.FloatField(verbose_name='Денег в кошельке')
    created_at = models.DateTimeField(auto_now_add=True,
                                      verbose_name='Создан')
    updated_at = models.DateTimeField(auto_now=True,
                                      verbose_name='Обновлен')


class Transaction(models.Model):
    id_transaction = models.AutoField(primary_key=True,
                                      verbose_name='id')
    user = models.ForeignKey(User,
                             on_delete=models.SET_NULL,
                             null=True,
                             related_name='transaction',
                             verbose_name='Пользователь')
    cash = models.PositiveIntegerField(verbose_name='Денег для перевода')
    created_at = models.DateTimeField(auto_now_add=True,
                                      verbose_name='Создан')


class Question(models.Model):
    id_question = models.AutoField(primary_key=True,
                                   verbose_name='id')
    user = models.ForeignKey(User,
                             on_delete=models.SET_NULL,
                             null=True,
                             related_name='question',
                             verbose_name='Пользователь')
    text_question = models.TextField(verbose_name='Текст вопроса', null=True)
    file_question = models.TextField(max_length=100, verbose_name='файл вопроса',
                                     null=True)
    type_question = models.CharField(max_length=10, verbose_name='Тип вопроса',
                                     null=True)
    text_ask = models.TextField(verbose_name='Текст ответа',
                                null=True)
    file_ask = models.TextField(max_length=100, verbose_name='файл ответа',
                                null=True)
    type_ask = models.CharField(max_length=10, verbose_name='Тип ответа',
                                null=True)
    plan_id = models.PositiveIntegerField(verbose_name='id плана',
                                          null=True)
    status_send = models.BooleanField(default=False, verbose_name='Статус отправки')
    status_ask = models.BooleanField(default=False, verbose_name='Статус ответа')
    created_at = models.DateTimeField(auto_now_add=True,
                                      verbose_name='Создан')
    updated_at = models.DateTimeField(auto_now=True,
                                      verbose_name='Обновлен')

    class Meta:
        verbose_name = 'Вопрос'
        verbose_name_plural = 'Вопросы'

    def __str__(self):
        return f'{self.user} {self.user}'
