# Generated by Django 5.0.3 on 2024-03-26 13:00

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('Users', '0004_remove_question_status_question_status_ask_and_more'),
    ]

    operations = [
        migrations.AddField(
            model_name='question',
            name='file_ask',
            field=models.TextField(max_length=100, null=True, verbose_name='файл ответа'),
        ),
        migrations.AddField(
            model_name='question',
            name='file_question',
            field=models.TextField(max_length=100, null=True, verbose_name='файл вопроса'),
        ),
        migrations.AddField(
            model_name='question',
            name='type_ask',
            field=models.CharField(max_length=10, null=True, verbose_name='Тип ответа'),
        ),
        migrations.AddField(
            model_name='question',
            name='type_question',
            field=models.CharField(max_length=10, null=True, verbose_name='Тип вопроса'),
        ),
        migrations.AlterField(
            model_name='question',
            name='text_question',
            field=models.TextField(null=True, verbose_name='Текст вопроса'),
        ),
    ]
