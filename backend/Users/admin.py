from django.contrib import admin
from .models import User, Wallet, Transaction, Question


class UserAdmin(admin.ModelAdmin):
    list_display = ['user_id',
                    'first_name',
                    'last_name',
                    'username',
                    'created_at',
                    'updated_at',
                    'is_admin', ]
    list_display_links = ['is_admin', ]


class WalletAdmin(admin.ModelAdmin):
    list_display = ['user',
                    'cash',
                    'created_at',
                    'updated_at']
    list_display_links = ['cash']


class TransactionAdmin(admin.ModelAdmin):
    list_display = ['id_transaction',
                    'user',
                    'cash',
                    'created_at', ]


class QuestionAdmin(admin.ModelAdmin):
    list_display = ['id_question',
                    'user',
                    'text_question',
                    'text_ask',
                    'plan_id',
                    'status_send',
                    'status_ask',
                    'updated_at',
                    'created_at']
    list_display_links = ['status_ask']


admin.site.register(User, UserAdmin)
admin.site.register(Wallet, WalletAdmin)
admin.site.register(Transaction, TransactionAdmin)
admin.site.register(Question, QuestionAdmin)
