**********Телеграмм бот для ответы на вопросы**********

1. Создаем образ bot_lawyer
   * `docker build -f Dockerfile_bot -t bot_lawyer .`

2. Создаем образ web_bot_lawyer
   * `docker build -f Dockerfile_web -t web_bot_lawyer .`

3. Создаем файл .env
   * `nano .env`
               _POSTGRES_USER=
               POSTGRES_PASSWORD=
               POSTGRES_HOST=localhost
               POSTGRES_PORT=5432
               POSTGRES_DB=Bot_lawyer_

4. Создаем 2-ой файл .env
   * `nano backend/.env`
               _BOT_TOKEN=
               POSTGRES_USER=
               POSTGRES_PASSWORD=
               POSTGRES_HOST=postgres_bot_lawyer
               POSTGRES_PORT=5432
               POSTGRES_DB=Bot_lawyer_
               _PAYMENT_API_KEY=
               PAYMENT_API_ID=
               PAYMENT_SECRET_KEY=
               PAYMENT_SHOP=_

5. Запускаем docker compose
   *  `docker compose up -d`







