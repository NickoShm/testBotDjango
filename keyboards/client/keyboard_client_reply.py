from aiogram.utils.keyboard import ReplyKeyboardBuilder


def board_main_menu():
    keyboard = ReplyKeyboardBuilder()

    keyboard.button(text='ℹ️ Показать меню')
    keyboard.adjust(1)
    return keyboard.as_markup(resize_keyboard=True)
