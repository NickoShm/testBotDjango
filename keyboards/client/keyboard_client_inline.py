from aiogram.utils.keyboard import InlineKeyboardBuilder
from aiogram.types import InlineKeyboardButton, WebAppInfo
from datetime import timedelta


def board_main_menu(button_ask_question: str, button_my_ask: str,
                    button_balance: str, button_support: str,
                    my_ask: int, balance: int):
    keyboard = InlineKeyboardBuilder()

    keyboard.button(text=button_ask_question, callback_data='Задать вопрос')
    keyboard.button(text=f'{button_my_ask} ({my_ask})', callback_data='Мои вопросы')
    keyboard.button(text=f'{button_balance} (${balance})', callback_data='Баланс')
    keyboard.button(text=button_support, callback_data='Поддержка')

    keyboard.adjust(2, 1)
    return keyboard.as_markup(resize_keyboard=True)


def back(text):
    keyboard = InlineKeyboardBuilder()
    keyboard.button(text=text, callback_data='способ оплаты_назад')
    keyboard.adjust(1)
    return keyboard.as_markup(resize_keyboard=True)


def back_main_menu(text):
    keyboard = InlineKeyboardBuilder()
    keyboard.button(text=text, callback_data='Главное меню_no')
    keyboard.adjust(1)
    return keyboard.as_markup(resize_keyboard=True)


def back_send(text):
    keyboard = InlineKeyboardBuilder()
    keyboard.button(text=text, callback_data='назад отправить')
    keyboard.adjust(1)
    return keyboard.as_markup(resize_keyboard=True)


def response_time(plans, button_change_ask,
                  button_ask_chancel):
    keyboard = InlineKeyboardBuilder()
    for plan in plans:
        keyboard.row(InlineKeyboardButton(
            text=f'{plan.name} ({plan.price})',
            callback_data=f'{plan.name}_{plan.price}_{plan.id_plan}'))

    keyboard.row(InlineKeyboardButton(
        text=button_change_ask, callback_data='Изменить текст вопроса'))
    keyboard.row(InlineKeyboardButton(
        text=button_ask_chancel, callback_data='Отменить вопрос'))
    keyboard.adjust(1)
    return keyboard.as_markup(resize_keyboard=True)


def response_time_back(plans, data: str, button_back):
    keyboard = InlineKeyboardBuilder()
    for plan in plans:
        if data == plan.name:
            keyboard.row(InlineKeyboardButton(
                text=f'✅ {plan.name} ({plan.price})',
                callback_data=f'{plan.name}_{plan.price}_{plan.id_plan}'))
        else:
            keyboard.row(InlineKeyboardButton(
                text=f'{plan.name} ({plan.price})',
                callback_data=f'{plan.name}_{plan.price}_{plan.id_plan}'))
    keyboard.row(InlineKeyboardButton(text=button_back, callback_data='назад'))
    keyboard.adjust(1)
    return keyboard.as_markup(resize_keyboard=True)


def response_time_main_menu(plans, data: str, button_back):
    keyboard = InlineKeyboardBuilder()
    for plan in plans:
        if data == plan.name:
            keyboard.row(InlineKeyboardButton(
                text=f'✅ {plan.name} ({plan.price})',
                callback_data=f'{plan.name}_{plan.price}_{plan.id_plan}'))
        else:
            keyboard.row(InlineKeyboardButton(
                text=f'{plan.name} ({plan.price})',
                callback_data=f'{plan.name}_{plan.price}_{plan.id_plan}'))
    keyboard.button(text=button_back, callback_data='Главное меню_no')
    keyboard.adjust(1)
    return keyboard.as_markup(resize_keyboard=True)


def send_question(button_change_ask, button_change_time_question,
                  button_del_question, button_back, button_send, check):
    keyboard = InlineKeyboardBuilder()
    keyboard.button(text=button_change_ask, callback_data='Изменить текст вопроса')
    keyboard.button(text=button_change_time_question, callback_data='Изменить время ответа')
    keyboard.button(text=button_del_question, callback_data='Удалить вопрос')
    if check == 'вопрос':
        keyboard.button(text=button_back, callback_data='Мои вопросы')
    else:
        keyboard.button(text=button_back, callback_data='назад')
    keyboard.button(text=button_send, callback_data='Отправить')
    keyboard.adjust(1, 1, 1, 2)
    return keyboard.as_markup(resize_keyboard=True)


def del_question(button_change_ask, button_change_time_question,
                 button_restore_ask, button_show_menu):
    keyboard = InlineKeyboardBuilder()
    keyboard.button(text=button_change_ask, callback_data='Изменить текст вопроса')
    keyboard.button(text=button_change_time_question, callback_data='Изменить время ответа')
    keyboard.button(text=button_restore_ask, callback_data='Восстановить вопрос')
    keyboard.button(text=button_show_menu, callback_data='Главное меню_del')
    keyboard.adjust(1, 1, 1, 2)
    return keyboard.as_markup(resize_keyboard=True)


def contact_and_back(contact_text, contact_url, back_):
    keyboard = InlineKeyboardBuilder()

    keyboard.button(text=contact_text, url=contact_url)
    keyboard.button(text=back_, callback_data='Главное меню_no')
    keyboard.adjust(1)
    return keyboard.as_markup(resize_keyboard=True)


def back_and_user_ask(asks_user, button_back):
    keyboard = InlineKeyboardBuilder()
    for ask in asks_user:
        date_question = ask.created_at
        moscow_time = date_question + timedelta(hours=3)
        date_question_format = moscow_time.strftime("%d.%m.%Y в %H:%M")
        if ask.status_send is True and ask.status_ask is False:
            keyboard.row(InlineKeyboardButton(
                text=f'⏳ Вопрос №{ask.id_question} ({date_question_format})',
                callback_data=f'вопрос отправлен_{ask.id_question}'))
        elif ask.status_send is True and ask.status_ask is True:
            keyboard.row(InlineKeyboardButton(
                text=f'✅ Вопрос №{ask.id_question} ({date_question_format})',
                callback_data=f'вопрос закрыт_{ask.id_question}'))
        else:
            keyboard.row(InlineKeyboardButton(
                text=f'Вопрос №{ask.id_question} ({date_question_format})',
                callback_data=f'вопрос_{ask.id_question}'))
    keyboard.button(text=button_back, callback_data='Главное меню_no')
    keyboard.adjust(1)
    return keyboard.as_markup(resize_keyboard=True)


def pay_method(methods, button_back):
    keyboard = InlineKeyboardBuilder()
    for method in methods:
        keyboard.button(text=method.text, callback_data=f'способ оплаты_{method.callback}')
    keyboard.button(text=button_back, callback_data='Главное меню_no')
    keyboard.adjust(1)
    return keyboard.as_markup(resize_keyboard=True)


def summa_pay(button_back, summa_pay_1, summa_pay_2, summa_pay_3, summa_pay_4):
    keyboard = InlineKeyboardBuilder()
    keyboard.button(text=f'${summa_pay_1}', callback_data=f'сумма_{summa_pay_1}')
    keyboard.button(text=f'${summa_pay_2}', callback_data=f'сумма_{summa_pay_2}')
    keyboard.button(text=f'${summa_pay_3}', callback_data=f'сумма_{summa_pay_3}')
    keyboard.button(text=f'{summa_pay_4}', callback_data=f'сумма_ввод')
    keyboard.button(text=f'{button_back}', callback_data='Баланс')
    keyboard.adjust(3, 1)
    return keyboard.as_markup(resize_keyboard=True)


def pay(url):
    keyboard = InlineKeyboardBuilder()
    keyboard.button(text='Оплата', web_app=WebAppInfo(url=url))
    keyboard.adjust(1)
    return keyboard.as_markup(resize_keyboard=True)
