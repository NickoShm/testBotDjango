from aiogram.types import BotCommand


commands_list = [
    BotCommand(command='start', description='ℹ️ Главное меню'),
    BotCommand(command='ask', description='💬 Задать вопрос'),
    BotCommand(command='my', description='📝 Мои вопросы'),
    BotCommand(command='balance', description='💰 Баланс'),
    BotCommand(command='help', description='🆘 Поддержка')
]