from aiogram.utils.keyboard import ReplyKeyboardBuilder
from aiogram.types import InlineKeyboardButton


def questions():
    keyboard = ReplyKeyboardBuilder()
    keyboard.button(text='Вопросы')

    keyboard.adjust(1)
    return keyboard.as_markup(resize_keyboard=True)


def back():
    keyboard = InlineKeyboardBuilder()
    keyboard.button(text='Назад', callback_data='назад admin')
    keyboard.adjust(1)
    return keyboard.as_markup(resize_keyboard=True)
