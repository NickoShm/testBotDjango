from aiogram.utils.keyboard import InlineKeyboardBuilder
from aiogram.types import InlineKeyboardButton


def ask(id_question):
    keyboard = InlineKeyboardBuilder()
    keyboard.button(text='Ответить', callback_data=f'ответить_{id_question}')

    keyboard.adjust(1)
    return keyboard.as_markup(resize_keyboard=True)


def back():
    keyboard = InlineKeyboardBuilder()
    keyboard.button(text='Назад', callback_data='назад admin')
    keyboard.adjust(1)
    return keyboard.as_markup(resize_keyboard=True)
