from aiogram.types import CallbackQuery, Message
from handlers.client.main import router_client
from aiogram import F
from keyboards.client import keyboard_client_inline
from aiogram.fsm.context import FSMContext
from aiogram.fsm.state import State, StatesGroup
from aiogram.filters import Command
from data_base import base_components, base_wallet, base_transactions
from sqlalchemy.ext.asyncio import AsyncSession
from app_payok.api import Payok
# from config import app_config
import os

# api_key = app_config.payment.api_key.get_secret_value()
# api_id = app_config.payment.api_id
# secret_key = app_config.payment.secret_key.get_secret_value()
# shop = app_config.payment.shop

# payok = Payok(api_id=api_id, api_key=api_key, secret_key=secret_key, shop=shop)
payok = Payok(api_id=int(os.getenv('PAYMENT_API_ID')),
              api_key=os.getenv('PAYMENT_API_KEY'),
              secret_key=os.getenv('PAYMENT_SECRET_KEY'),
              shop=os.getenv('PAYMENT_SHOP'))


class FsmPay(StatesGroup):
    pay = State()
    get_cash = State()


@router_client.message(Command('balance'))
async def balance_command(message: Message, session: AsyncSession):
    """
    Функция пополнения баланса
    :param session: AsyncSession
    :param message: Message
    :return:
    """
    balance = await base_wallet.get_wallet_by_user_id(session, message.from_user.id)
    mes = await base_components.get_message_by_id(session, 9)
    button_back = await base_components.get_button_by_id(session, 5)
    button_pay_method = await base_components.get_method_pay(session)
    await message.answer(
        f'Ваш баланс: <b>${balance}</b>'
        f'\n\n----------------------------------'
        f'\n{mes}',
        reply_markup=keyboard_client_inline.pay_method(
            button_pay_method, button_back
        )
    )


@router_client.callback_query(F.data == 'Баланс')
async def balance_callback(callback: CallbackQuery, session: AsyncSession):
    """
    Функция пополнения баланса инлайн
    :param callback: callback
    :param session: AsyncSession
    :return:
    """
    balance = await base_wallet.get_wallet_by_user_id(session, callback.from_user.id)
    mes = await base_components.get_message_by_id(session, 9)
    button_back = await base_components.get_button_by_id(session, 5)
    button_pay_method = await base_components.get_method_pay(session)
    await callback.message.edit_text(
        f'Ваш баланс: <b>${balance}</b>'
        f'\n\n----------------------------------'
        f'\n{mes}',
        reply_markup=keyboard_client_inline.pay_method(
            button_pay_method, button_back
        )
    )


@router_client.callback_query(F.data.startswith('способ оплаты_'))
async def get_method_pay(callback: CallbackQuery, session: AsyncSession,
                         state: FSMContext):
    """
    Функция получает способ оплаты
    :param state: FSMContext
    :param callback: callback
    :param session: AsyncSession
    :return:
    """
    await state.set_state(FsmPay.pay)
    mes = await base_components.get_message_by_id(session, 11)
    method_pay = callback.data.split('_')[1]
    button_back = await base_components.get_button_by_id(session, 5)
    summa_pay_1 = await base_components.get_button_by_id(session, 15)
    summa_pay_2 = await base_components.get_button_by_id(session, 16)
    summa_pay_3 = await base_components.get_button_by_id(session, 17)
    summa_pay_4 = await base_components.get_button_by_id(session, 18)
    if method_pay != 'назад':
        await state.update_data(method_pay=method_pay)
    await callback.message.edit_text(
        mes,
        reply_markup=keyboard_client_inline.summa_pay(button_back, summa_pay_1,
                                                      summa_pay_2, summa_pay_3,
                                                      summa_pay_4)
    )


@router_client.callback_query(F.data.startswith('сумма_'))
async def get_summa_pay(callback: CallbackQuery, session: AsyncSession,
                        state: FSMContext):
    """
    Функция получает сумму платежа
    :param state: FSMContext
    :param callback: callback
    :param session: AsyncSession
    :return:
    """
    summa = callback.data.split('_')[1]
    if summa != 'ввод':
        summa = float(summa)
        context_data = await state.get_data()
        method_pay = context_data.get('method_pay')
        transaction_id = await base_transactions.add_transaction(session,
                                                                 callback.from_user.id,
                                                                 summa)
        pay = await payok.create_pay(
            amount=summa,
            payment=transaction_id,
            currency='USD',
            desc='Описание платежа',
            method=method_pay,
            lang='RU',
        )
        await callback.message.edit_text(
            f'Вы собираетесь пополнить баланс на сумму ${summa} '
            '\n\nпожалуйста, перейдите по ссылке ниже, чтобы завершить платеж 👇',
            reply_markup=keyboard_client_inline.pay(pay)
        )
        return
    button_back = await base_components.get_button_by_id(session, 5)
    mes = await base_components.get_message_by_id(session, 11)
    await state.set_state(FsmPay.get_cash)
    await callback.message.edit_text(
        mes,
        reply_markup=keyboard_client_inline.back(button_back)
    )


@router_client.message(FsmPay.get_cash)
async def get_cash(message: Message, session: AsyncSession,
                   state: FSMContext):
    """
    Функция пополнения баланса
    :param state: FSMContext
    :param session: AsyncSession
    :param message: Message
    :return:
    """
    summa = message.text
    try:
        summa = float(summa)
    except ValueError:
        button_back = await base_components.get_button_by_id(session, 5)
        summa_pay_1 = await base_components.get_button_by_id(session, 16)
        summa_pay_2 = await base_components.get_button_by_id(session, 17)
        summa_pay_3 = await base_components.get_button_by_id(session, 18)
        summa_pay_4 = await base_components.get_button_by_id(session, 19)
        mes = await base_components.get_message_by_id(session, 12)
        await message.answer(mes, reply_markup=keyboard_client_inline.summa_pay(button_back, summa_pay_1,
                                                                                summa_pay_2, summa_pay_3,
                                                                                summa_pay_4))
        return
    context_data = await state.get_data()
    method_pay = context_data.get('method_pay')
    transaction_id = await base_transactions.add_transaction(session,
                                                             message.from_user.id,
                                                             summa)
    pay = await payok.create_pay(
        amount=summa,
        payment=transaction_id,
        currency='USD',
        desc='Описание платежа',
        method=method_pay,
        lang='RU',
    )
    await message.delete()
    await message.answer(
        f'<b>Вы собираетесь пополнить баланс на сумму ${summa:.2f}</b>'
        '\n\nПожалуйста, перейдите по ссылке ниже, чтобы завершить платеж 👇',
        reply_markup=keyboard_client_inline.pay(pay)
    )
