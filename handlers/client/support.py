from aiogram.types import CallbackQuery, Message
from handlers.client.main import router_client
from aiogram import F, Bot
from keyboards.client import keyboard_client_inline
from aiogram.fsm.context import FSMContext
from aiogram.fsm.state import State, StatesGroup
from datetime import datetime
from aiogram.filters import Command
from data_base import base_components, base_question, base_wallet
from sqlalchemy.ext.asyncio import AsyncSession


@router_client.message(Command('help'))
async def support_command(message: Message, session: AsyncSession):
    """
    Функция поддержка
    :param session: AsyncSession
    :param message: Message
    :return:
    """
    mes = await base_components.get_message_by_id(session, 4)
    button_contact_text = await base_components.get_button_by_id(session, 13)
    button_contact_url = await base_components.get_button_by_id(session, 14)
    button_back = await base_components.get_button_by_id(session, 5)
    await message.answer(
        mes,
        reply_markup=keyboard_client_inline.contact_and_back(button_contact_text,
                                                             button_contact_url,
                                                             button_back)
    )


@router_client.callback_query(F.data == 'Поддержка')
async def support_callback(callback: CallbackQuery, session: AsyncSession):
    """
    Функция поддержка инлайн
    :param callback: callback
    :param session: AsyncSession
    :return:
    """
    mes = await base_components.get_message_by_id(session, 4)
    button_contact_text = await base_components.get_button_by_id(session, 13)
    button_contact_url = await base_components.get_button_by_id(session, 14)
    button_back = await base_components.get_button_by_id(session, 5)
    await callback.message.edit_text(
        mes,
        reply_markup=keyboard_client_inline.contact_and_back(button_contact_text,
                                                             button_contact_url,
                                                             button_back)
    )
