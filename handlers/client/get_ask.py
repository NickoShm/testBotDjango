from aiogram.types import CallbackQuery, Message
from handlers.client.main import router_client
from aiogram import F
from keyboards.client import keyboard_client_inline
from aiogram.filters import Command
from data_base import base_components, base_question
from sqlalchemy.ext.asyncio import AsyncSession


@router_client.message(Command('my'))
async def get_ask_command(message: Message, session: AsyncSession):
    """
    Функция мои вопросы
    :param session: AsyncSession
    :param message: Message
    :return:
    """
    mes = await base_components.get_message_by_id(session, 5)
    asks_user = await base_question.get_question_by_user_id(session, message.from_user.id)
    if not asks_user:
        mes_1 = await base_components.get_message_by_id(session, 7)
        await message.answer(mes_1)
        return
    button_back = await base_components.get_button_by_id(session, 5)
    await message.answer(
        mes,
        reply_markup=keyboard_client_inline.back_and_user_ask(asks_user,
                                                              button_back)
    )


@router_client.callback_query(F.data == 'Мои вопросы')
async def get_ask_callback(callback: CallbackQuery, session: AsyncSession):
    """
    Функция мои вопросы инлайн
    :param callback: callback
    :param session: AsyncSession
    :return:
    """
    mes = await base_components.get_message_by_id(session, 5)
    asks_user = await base_question.get_question_by_user_id(session, callback.from_user.id)
    if not asks_user:
        mes_1 = await base_components.get_message_by_id(session, 7)
        await callback.answer(mes_1)
        return
    button_back = await base_components.get_button_by_id(session, 5)
    await callback.message.edit_text(
        mes,
        reply_markup=keyboard_client_inline.back_and_user_ask(asks_user,
                                                              button_back)
    )
