from aiogram.types import CallbackQuery, Message
from handlers.client.main import router_client
from aiogram import F
from keyboards.client import keyboard_client_inline
from aiogram.filters import Command
from data_base import base_components, base_question
from sqlalchemy.ext.asyncio import AsyncSession


@router_client.callback_query(F.data.startswith('вопрос закрыт_'))
async def get_ask_callback(callback: CallbackQuery, session: AsyncSession):
    """
    Функция мои вопросы инлайн
    :param callback: callback
    :param session: AsyncSession
    :return:
    """
    id_question = int(callback.data.split('_')[1])
    question = await base_question.get_question_by_id(session, id_question)
    button_back = await base_components.get_button_by_id(session, 5)
    await callback.message.edit_text(
        f'<b>Ваш вопрос:</b>'
        f'\n{question.text_question}'
        f'\n\n<b>Ответ:</b>'
        f'\n{question.text_ask}',
        reply_markup=keyboard_client_inline.back_main_menu(button_back)
    )


@router_client.callback_query(F.data.startswith('вопрос отправлен_'))
async def get_ask_callback(callback: CallbackQuery, session: AsyncSession):
    """
    Функция мои вопросы инлайн
    :param callback: callback
    :param session: AsyncSession
    :return:
    """
    id_question = int(callback.data.split('_')[1])
    question = await base_question.get_question_by_id(session, id_question)
    button_back = await base_components.get_button_by_id(session, 5)
    await callback.message.edit_text(
        f'<b>Ваш вопрос:</b>'
        f'\n{question.text_question}'
        f'\n\n<b>Статус:</b> вопрос отправлен. Ожидайте ответа.',
        reply_markup=keyboard_client_inline.back_main_menu(button_back)
    )