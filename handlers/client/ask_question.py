from aiogram.types import CallbackQuery, Message
from handlers.client.main import router_client, command_start_callback
from aiogram import F, Bot, types
from keyboards.client import keyboard_client_inline
from keyboards.admin import keyboard_admin_inlain
from aiogram.fsm.context import FSMContext
from aiogram.fsm.state import State, StatesGroup
from datetime import datetime
from aiogram.filters import Command
from data_base import (base_components, base_question,
                       base_wallet, base_plan, base_chats)
from sqlalchemy.ext.asyncio import AsyncSession
from aiogram.exceptions import TelegramBadRequest, TelegramForbiddenError
from handlers.client.get_ask import get_ask_callback


class FsmAskQuestion(StatesGroup):
    """
    question - вопрос
    response_time - время ответа
    send_question - подтверждение отправки вопроса
    """
    question = State()
    response_time = State()
    send_question = State()


@router_client.message(Command('ask'))
async def ask_question_command(message: Message, state: FSMContext,
                               session: AsyncSession):
    """
    Функция задать вопрос (команда)
    :param session: AsyncSession
    :param state: FSMContext
    :param message: Message
    :return:
    """
    mes = await base_components.get_message_by_id(session, 2)
    button_back = await base_components.get_button_by_id(session, 5)
    await state.set_state(FsmAskQuestion.question)
    await message.answer(
        mes,
        reply_markup=keyboard_client_inline.back(button_back)
    )


@router_client.callback_query(F.data == 'Задать вопрос')
async def ask_question_funk(callback: CallbackQuery, state: FSMContext,
                            session: AsyncSession):
    """
    Функция задать вопрос
    :param session: AsyncSession
    :param state: FSMContext
    :param callback: CallbackQuery
    :return:
    """
    mes = await base_components.get_message_by_id(session, 2)
    button_back = await base_components.get_button_by_id(session, 5)
    await state.set_state(FsmAskQuestion.question)
    await state.update_data(mes_id=callback.message.message_id)
    try:
        await callback.message.edit_text(
            mes,
            reply_markup=keyboard_client_inline.back(button_back)
        )
    except TelegramBadRequest:
        await callback.message.delete()
        mes = await callback.message.answer(
            mes,
            reply_markup=keyboard_client_inline.back(button_back)
        )
        await state.update_data(mes_id=mes.message_id)


@router_client.callback_query(F.data.startswith('Главное меню'))
@router_client.callback_query(FsmAskQuestion.question, F.data == 'назад')
async def return_to_start(callback: CallbackQuery, state: FSMContext,
                          session: AsyncSession):
    """
    Функция возврата в главное меню
    (иглайн)
    :param session: AsyncSession
    :param callback: CallbackQuery
    :param state: FSMContext
    :return:
    """
    button_ask_question = await base_components.get_button_by_id(session, 1)
    button_my_ask = await base_components.get_button_by_id(session, 2)
    button_balance = await base_components.get_button_by_id(session, 3)
    button_support = await base_components.get_button_by_id(session, 4)

    wallet = await base_wallet.get_wallet_by_user_id(session, callback.from_user.id)
    context_data = await state.get_data()
    if callback.data.startswith('Главное меню'):
        if callback.data.split('_')[1] == 'del':
            id_question = context_data.get('id_question')
            await base_question.del_question(session, id_question)
        await state.clear()
        mes = await base_components.get_message_by_id(session, 3)
        my_ask = await base_question.get_count_question_user(session, callback.from_user.id)
        await callback.message.edit_text(
            mes,
            reply_markup=keyboard_client_inline.board_main_menu(
                button_ask_question, button_my_ask, button_balance,
                button_support, my_ask, wallet
            )
        )
        return

    if context_data.get('response_time') is not None:
        await get_response_time(callback, state, session)
        return
    await state.clear()
    my_ask = await base_question.get_count_question_user(session, callback.from_user.id)
    mes = await base_components.get_message_by_id(session, 3)
    if context_data.get('file') is None:
        await callback.message.edit_text(
            mes,
            reply_markup=keyboard_client_inline.board_main_menu(
                button_ask_question, button_my_ask, button_balance,
                button_support, my_ask, wallet
            )
        )
        return
    await callback.message.edit_caption(
        caption=mes,
        reply_markup=keyboard_client_inline.board_main_menu(
            button_ask_question, button_my_ask, button_balance,
            button_support, my_ask, wallet
        )
    )


@router_client.message(FsmAskQuestion.question)
async def get_question(message: Message, state: FSMContext,
                       bot: Bot, session: AsyncSession):
    """
    Функция получения вопроса
    выбор времени ответа
    :param session: AsyncSession
    :param message: Message
    :param bot: Bot
    :param state: FSMContext
    :return:
    """
    context_data = await state.get_data()
    mes_id = context_data.get('mes_id')
    if message.content_type == types.ContentType.TEXT and len(message.text) < 11:
        mes = await base_components.get_message_by_id(session, 13)
        await bot.edit_message_text(
            chat_id=message.from_user.id,
            message_id=mes_id,
            text=mes
        )
        return
    date_question = datetime.now()
    date_question_format = date_question.strftime("%d.%m.%Y в %H:%M")
    id_question = context_data.get('id_question')
    if id_question is None:
        if message.content_type == types.ContentType.TEXT:
            id_question = await base_question.add_question(session, message.from_user.id,
                                                           message.text, None, None)
            await state.update_data(id_question=id_question,
                                    question=message.text,
                                    date_question=date_question)
        else:
            if message.content_type == types.ContentType.PHOTO:
                file = message.photo[-1].file_id
            elif message.content_type == types.ContentType.VIDEO:
                file = message.video.file_id
            elif message.content_type == types.ContentType.DOCUMENT:
                file = message.document.file_id
            else:
                file = message.voice.file_id
            id_question = await base_question.add_question(session, message.from_user.id,
                                                           message.caption, message.content_type, file)
            await state.update_data(id_question=id_question, file=file,
                                    question=message.caption,
                                    file_type=message.content_type,
                                    date_question=date_question)
    else:
        if message.content_type == types.ContentType.TEXT:
            await base_question.update_question_question(session, id_question,
                                                         message.text)
            await state.update_data(id_question=id_question,
                                    question=message.text,
                                    date_question=date_question)
        else:
            if message.content_type == types.ContentType.PHOTO:
                file = message.photo[-1].file_id
            elif message.content_type == types.ContentType.VIDEO:
                file = message.video.file_id
            elif message.content_type == types.ContentType.DOCUMENT:
                file = message.document.file_id
            else:
                file = message.voice.file_id
                await base_question.update_question_question_file(session, id_question,
                                                                  message.content_type, file)

            await state.update_data(id_question=id_question, file=file,
                                    question=message.caption,
                                    file_type=message.content_type,
                                    date_question=date_question)
    await bot.delete_message(chat_id=message.chat.id, message_id=mes_id)
    response_time = context_data.get('response_time')
    status = context_data.get('status')
    button_change_ask = await base_components.get_button_by_id(session, 6)
    if response_time is not None:
        button_change_time_question = await base_components.get_button_by_id(session, 7)
        button_del_question = await base_components.get_button_by_id(session, 8)
        button_back = await base_components.get_button_by_id(session, 5)
        button_send = await base_components.get_button_by_id(session, 9)
        await state.set_state(FsmAskQuestion.send_question)
        if status == 'delete':
            button_restore_ask = await base_components.get_button_by_id(session, 10)
            button_show_menu = await base_components.get_button_by_id(session, 11)
            if message.content_type == types.ContentType.TEXT:
                await message.answer(
                    f'<b>Вопрос от {date_question_format}</b>'
                    f'\n\n<b>Текст:</b>'
                    f'\n<code>{message.text}</code>'
                    f'\n\n<b>Время ответа:</b> {response_time}'
                    f'\n<b>Статус:</b> не отправлен',
                    reply_markup=keyboard_client_inline.del_question(
                        button_change_ask, button_change_time_question,
                        button_restore_ask, button_show_menu
                    )
                )
                return
            if message.content_type == types.ContentType.PHOTO:
                await message.answer_photo(
                    photo=file,
                    caption=f'<b>Вопрос от {date_question_format}</b>'
                            f'\n\n<b>Текст:</b>'
                            f'\n<code>{message.caption}</code>'
                            f'\n\n<b>Время ответа:</b> {response_time}'
                            f'\n<b>Статус:</b> не отправлен',
                    reply_markup=keyboard_client_inline.del_question(
                        button_change_ask, button_change_time_question,
                        button_restore_ask, button_show_menu
                    )
                )
                return
            if message.content_type == types.ContentType.VIDEO:
                await message.answer_video(
                    video=file,
                    caption=f'<b>Вопрос от {date_question_format}</b>'
                            f'\n\n<b>Текст:</b>'
                            f'\n<code>{message.caption}</code>'
                            f'\n\n<b>Время ответа:</b> {response_time}'
                            f'\n<b>Статус:</b> не отправлен',
                    reply_markup=keyboard_client_inline.del_question(
                        button_change_ask, button_change_time_question,
                        button_restore_ask, button_show_menu
                    )
                )
                return
            if message.content_type == types.ContentType.DOCUMENT:
                await message.answer_document(
                    document=file,
                    caption=f'<b>Вопрос от {date_question_format}</b>'
                            f'\n\n<b>Текст:</b>'
                            f'\n<code>{message.caption}</code>'
                            f'\n\n<b>Время ответа:</b> {response_time}'
                            f'\n<b>Статус:</b> не отправлен',
                    reply_markup=keyboard_client_inline.del_question(
                        button_change_ask, button_change_time_question,
                        button_restore_ask, button_show_menu
                    )
                )
                return
            if message.content_type == types.ContentType.VOICE:
                await message.answer_voice(
                    voice=file,
                    caption=f'<b>Вопрос от {date_question_format}</b>'
                            f'\n\n<b>Текст:</b>'
                            f'\n<code>{message.caption}</code>'
                            f'\n\n<b>Время ответа:</b> {response_time}'
                            f'\n<b>Статус:</b> не отправлен',
                    reply_markup=keyboard_client_inline.del_question(
                        button_change_ask, button_change_time_question,
                        button_restore_ask, button_show_menu
                    )
                )
                return
        if message.content_type == types.ContentType.TEXT:
            await message.answer(
                f'<b>Вопрос от {date_question_format}</b>'
                f'\n\n<b>Текст:</b>'
                f'\n<code>{message.text}</code>'
                f'\n\n<b>Время ответа:</b> {response_time}'
                f'\n<b>Статус:</b> не отправлен',
                reply_markup=keyboard_client_inline.send_question(
                    button_change_ask, button_change_time_question,
                    button_del_question, button_back, button_send, None)
            )
            return
        if message.content_type == types.ContentType.PHOTO:
            await message.answer_photo(
                photo=file,
                caption=f'<b>Вопрос от {date_question_format}</b>'
                        f'\n\n<b>Текст:</b>'
                        f'\n<code>{message.caption}</code>'
                        f'\n\n<b>Время ответа:</b> {response_time}'
                        f'\n<b>Статус:</b> не отправлен',
                reply_markup=keyboard_client_inline.send_question(
                    button_change_ask, button_change_time_question,
                    button_del_question, button_back, button_send,
                    None
                )
            )
            return
        if message.content_type == types.ContentType.VIDEO:
            await message.answer_video(
                video=file,
                caption=f'<b>Вопрос от {date_question_format}</b>'
                        f'\n\n<b>Текст:</b>'
                        f'\n<code>{message.caption}</code>'
                        f'\n\n<b>Время ответа:</b> {response_time}'
                        f'\n<b>Статус:</b> не отправлен',
                reply_markup=keyboard_client_inline.send_question(
                    button_change_ask, button_change_time_question,
                    button_del_question, button_back, button_send,
                    None
                )
            )
            return
        if message.content_type == types.ContentType.DOCUMENT:
            await message.answer_document(
                document=file,
                caption=f'<b>Вопрос от {date_question_format}</b>'
                        f'\n\n<b>Текст:</b>'
                        f'\n<code>{message.caption}</code>'
                        f'\n\n<b>Время ответа:</b> {response_time}'
                        f'\n<b>Статус:</b> не отправлен',
                reply_markup=keyboard_client_inline.send_question(
                    button_change_ask, button_change_time_question,
                    button_del_question, button_back, button_send,
                    None
                )
            )
            return
        if message.content_type == types.ContentType.VOICE:
            await message.answer_voice(
                voice=file,
                caption=f'<b>Вопрос от {date_question_format}</b>'
                        f'\n\n<b>Текст:</b>'
                        f'\n<code>{message.caption}</code>'
                        f'\n\n<b>Время ответа:</b> {response_time}'
                        f'\n<b>Статус:</b> не отправлен',
                reply_markup=keyboard_client_inline.send_question(
                    button_change_ask, button_change_time_question,
                    button_del_question, button_back, button_send,
                    None
                )
            )
            return
    plans = await base_components.get_plans(session)
    button_ask_chancel = await base_components.get_button_by_id(session, 12)
    await state.set_state(FsmAskQuestion.response_time)
    if message.content_type == types.ContentType.TEXT:
        await message.answer(
            f'<b>Вопрос от {date_question_format}</b>'
            f'\n\n<b>Текст:</b>'
            f'\n<code>{message.text}</code>'
            f'\n\n\n---------------------------------'
            f'\nПожалуйста, выберите время ответа',
            reply_markup=keyboard_client_inline.response_time(
                plans, button_change_ask, button_ask_chancel)
        )
        return
    await message.delete()
    if message.content_type == types.ContentType.PHOTO:
        await message.answer_photo(
            photo=file,
            caption=f'<b>Вопрос от {date_question_format}</b>'
                    f'\n\n<b>Текст:</b>'
                    f'\n<code>{message.caption}</code>'
                    f'\n\n\n---------------------------------'
                    f'\nПожалуйста, выберите время ответа',
            reply_markup=keyboard_client_inline.response_time(
                plans, button_change_ask, button_ask_chancel)
        )
        return
    if message.content_type == types.ContentType.VIDEO:
        await message.answer_video(
            video=file,
            caption=f'<b>Вопрос от {date_question_format}</b>'
                    f'\n\n<b>Текст:</b>'
                    f'\n<code>{message.caption}</code>'
                    f'\n\n\n---------------------------------'
                    f'\nПожалуйста, выберите время ответа',
            reply_markup=keyboard_client_inline.response_time(
                plans, button_change_ask, button_ask_chancel)
        )
        return
    if message.content_type == types.ContentType.DOCUMENT:
        await message.answer_document(
            document=file,
            caption=f'<b>Вопрос от {date_question_format}</b>'
                    f'\n\n<b>Текст:</b>'
                    f'\n<code>{message.caption}</code>'
                    f'\n\n\n---------------------------------'
                    f'\nПожалуйста, выберите время ответа',
            reply_markup=keyboard_client_inline.response_time(
                plans, button_change_ask, button_ask_chancel)
        )
        return
    if message.content_type == types.ContentType.VOICE:
        await message.answer_voice(
            voice=file,
            caption=f'<b>Вопрос от {date_question_format}</b>'
                    f'\n\n<b>Текст:</b>'
                    f'\n<code>{message.caption}</code>'
                    f'\n\n\n---------------------------------'
                    f'\nПожалуйста, выберите время ответа',
            reply_markup=keyboard_client_inline.response_time(
                plans, button_change_ask, button_ask_chancel)
        )


@router_client.callback_query(F.data.startswith('назад отправить'))
@router_client.callback_query(F.data.startswith('вопрос_'))
@router_client.callback_query(FsmAskQuestion.response_time)
async def get_response_time(callback: CallbackQuery, state: FSMContext,
                            session: AsyncSession):
    """
    Функция получения времени ответа на вопрос
    :param session: AsyncSession
    :param callback: CallbackQuery
    :param state: FSMContext
    :return:
    """
    if callback.data == 'Изменить текст вопроса':
        await ask_question_funk(callback, state, session)
        return

    if callback.data == 'Отменить вопрос':
        await return_to_start(callback, state, session)
        return
    context_state = await state.get_state()
    if context_state is None:
        id_question = int(callback.data.split('_')[1])
        question = await base_question.get_question_by_id(session, id_question)
        plan_id = question.plan_id
        plan = await base_plan.get_plan_by_id(session, plan_id)
        response_time = None
        if plan is not None:
            response_time = plan.name
        date_question = question.created_at
        question_text = question.text_question
        question_file = question.file_question
        question_file_type = question.type_question
        await state.update_data(
            id_question=id_question,
            question=question_text,
            date_question=date_question,
            response_time=response_time,
            file=question_file,
            file_type=question_file_type,
            plan_id=plan_id)
    context_data = await state.get_data()
    date_question = context_data.get('date_question')
    date_question_format = date_question.strftime("%d.%m.%Y в %H:%M")
    question_text = context_data.get('question')
    status = context_data.get('status')
    file = context_data.get('file')
    file_type = context_data.get('file_type')
    if (callback.data != 'назад' and callback.data != 'назад отправить'
            and not callback.data.startswith('вопрос_')):
        id_question = context_data.get('id_question')
        response_time = callback.data.split('_')[0]
        plan_id = int(callback.data.split('_')[2])
        await base_question.update_question_plan(session, id_question,
                                                 plan_id)
        await state.update_data(response_time=response_time,
                                plan_id=plan_id)
    else:
        response_time = context_data.get('response_time')
    await state.set_state(FsmAskQuestion.send_question)

    if status is None:
        await state.update_data(status='active')
    button_change_ask = await base_components.get_button_by_id(session, 6)
    button_change_time_question = await base_components.get_button_by_id(session, 7)
    button_del_question = await base_components.get_button_by_id(session, 8)
    button_back = await base_components.get_button_by_id(session, 5)
    button_send = await base_components.get_button_by_id(session, 9)
    if status == 'delete':
        button_restore_ask = await base_components.get_button_by_id(session, 10)
        button_show_menu = await base_components.get_button_by_id(session, 11)
        if response_time is None:
            if file_type is None:
                await callback.message.edit_text(
                    f'<b>Вопрос от {date_question_format}</b>'
                    f'\n\n<b>Текст:</b>'
                    f'\n<code>{question_text}</code>',
                    reply_markup=keyboard_client_inline.del_question(
                        button_change_ask, button_change_time_question,
                        button_restore_ask, button_show_menu
                    )
                )
                return
            try:
                await callback.message.edit_caption(
                    caption=f'<b>Вопрос от {date_question_format}</b>'
                            f'\n\n<b>Текст:</b>'
                            f'\n<code>{question_text}</code>',
                    reply_markup=keyboard_client_inline.del_question(
                        button_change_ask, button_change_time_question,
                        button_restore_ask, button_show_menu
                    )
                )
                return
            except TelegramBadRequest:
                await callback.message.delete()
                if file_type == types.ContentType.PHOTO:
                    await callback.message.answer_photo(
                        photo=file,
                        caption=f'<b>Вопрос от {date_question_format}</b>'
                                f'\n\n<b>Текст:</b>'
                                f'\n<code>{question_text}</code>',
                        reply_markup=keyboard_client_inline.del_question(
                            button_change_ask, button_change_time_question,
                            button_restore_ask, button_show_menu
                        )
                    )
                    return
                if file_type == types.ContentType.VIDEO:
                    await callback.message.answer_video(
                        video=file,
                        caption=f'<b>Вопрос от {date_question_format}</b>'
                                f'\n\n<b>Текст:</b>'
                                f'\n<code>{question_text}</code>',
                        reply_markup=keyboard_client_inline.del_question(
                            button_change_ask, button_change_time_question,
                            button_restore_ask, button_show_menu
                        )
                    )
                    return
                if file_type == types.ContentType.DOCUMENT:
                    await callback.message.answer_document(
                        document=file,
                        caption=f'<b>Вопрос от {date_question_format}</b>'
                                f'\n\n<b>Текст:</b>'
                                f'\n<code>{question_text}</code>',
                        reply_markup=keyboard_client_inline.del_question(
                            button_change_ask, button_change_time_question,
                            button_restore_ask, button_show_menu
                        )
                    )
                    return
                if file_type == types.ContentType.VOICE:
                    await callback.message.answer_voice(
                        voice=file,
                        caption=f'<b>Вопрос от {date_question_format}</b>'
                                f'\n\n<b>Текст:</b>'
                                f'\n<code>{question_text}</code>',
                        reply_markup=keyboard_client_inline.del_question(
                            button_change_ask, button_change_time_question,
                            button_restore_ask, button_show_menu
                        )
                    )
                    return
        if file_type is None:
            await callback.message.edit_text(
                f'<b>Вопрос от {date_question_format}</b>'
                f'\n\n<b>Текст:</b>'
                f'\n<code>{question_text}</code>'
                f'\n\n<b>Время ответа:</b> {response_time}'
                f'\n<b>Статус:</b> не отправлен',
                reply_markup=keyboard_client_inline.del_question(
                    button_change_ask, button_change_time_question,
                    button_restore_ask, button_show_menu
                )
            )
            return
        try:
            await callback.message.edit_caption(
                caption=f'<b>Вопрос от {date_question_format}</b>'
                        f'\n\n<b>Текст:</b>'
                        f'\n<code>{question_text}</code>'
                        f'\n\n<b>Время ответа:</b> {response_time}'
                        f'\n<b>Статус:</b> не отправлен',
                reply_markup=keyboard_client_inline.del_question(
                    button_change_ask, button_change_time_question,
                    button_restore_ask, button_show_menu
                )
            )
            return
        except TelegramBadRequest:
            await callback.message.delete()
            if file_type == types.ContentType.PHOTO:
                await callback.message.answer_photo(
                    photo=file,
                    caption=f'<b>Вопрос от {date_question_format}</b>'
                            f'\n\n<b>Текст:</b>'
                            f'\n<code>{question_text}</code>'
                            f'\n\n<b>Время ответа:</b> {response_time}'
                            f'\n<b>Статус:</b> не отправлен',
                    reply_markup=keyboard_client_inline.del_question(
                        button_change_ask, button_change_time_question,
                        button_restore_ask, button_show_menu
                    )
                )
                return
            if file_type == types.ContentType.VIDEO:
                await callback.message.answer_video(
                    video=file,
                    caption=f'<b>Вопрос от {date_question_format}</b>'
                            f'\n\n<b>Текст:</b>'
                            f'\n<code>{question_text}</code>'
                            f'\n\n<b>Время ответа:</b> {response_time}'
                            f'\n<b>Статус:</b> не отправлен',
                    reply_markup=keyboard_client_inline.del_question(
                        button_change_ask, button_change_time_question,
                        button_restore_ask, button_show_menu
                    )
                )
                return
            if file_type == types.ContentType.DOCUMENT:
                await callback.message.answer_document(
                    document=file,
                    caption=f'<b>Вопрос от {date_question_format}</b>'
                            f'\n\n<b>Текст:</b>'
                            f'\n<code>{question_text}</code>'
                            f'\n\n<b>Время ответа:</b> {response_time}'
                            f'\n<b>Статус:</b> не отправлен',
                    reply_markup=keyboard_client_inline.del_question(
                        button_change_ask, button_change_time_question,
                        button_restore_ask, button_show_menu
                    )
                )
                return
            if file_type == types.ContentType.VOICE:
                await callback.message.answer_voice(
                    voice=file,
                    caption=f'<b>Вопрос от {date_question_format}</b>'
                            f'\n\n<b>Текст:</b>'
                            f'\n<code>{question_text}</code>'
                            f'\n\n<b>Время ответа:</b> {response_time}'
                            f'\n<b>Статус:</b> не отправлен',
                    reply_markup=keyboard_client_inline.del_question(
                        button_change_ask, button_change_time_question,
                        button_restore_ask, button_show_menu
                    )
                )
                return
    if response_time is None:
        if file_type is None:
            await callback.message.edit_text(
                f'<b>Вопрос от {date_question_format}</b>'
                f'\n\n<b>Текст:</b>'
                f'\n<code>{question_text}</code>',
                reply_markup=keyboard_client_inline.send_question(
                    button_change_ask, button_change_time_question,
                    button_del_question, button_back, button_send,
                    callback.data.split('_')[0]
                )
            )
            return
        try:
            await callback.message.edit_caption(
                caption='<b>Вопрос от {date_question_format}</b>'
                        f'\n\n<b>Текст:</b>'
                        f'\n<code>{question_text}</code>',
                reply_markup=keyboard_client_inline.send_question(
                    button_change_ask, button_change_time_question,
                    button_del_question, button_back, button_send,
                    callback.data.split('_')[0]
                )
            )
            return
        except TelegramBadRequest:
            await callback.message.delete()
            if file_type == types.ContentType.PHOTO:
                await callback.message.answer_photo(
                    photo=file,
                    caption='<b>Вопрос от {date_question_format}</b>'
                            f'\n\n<b>Текст:</b>'
                            f'\n<code>{question_text}</code>',
                    reply_markup=keyboard_client_inline.send_question(
                        button_change_ask, button_change_time_question,
                        button_del_question, button_back, button_send,
                        callback.data.split('_')[0]
                    )
                )
                return
            if file_type == types.ContentType.VIDEO:
                await callback.message.answer_video(
                    video=file,
                    caption='<b>Вопрос от {date_question_format}</b>'
                            f'\n\n<b>Текст:</b>'
                            f'\n<code>{question_text}</code>',
                    reply_markup=keyboard_client_inline.send_question(
                        button_change_ask, button_change_time_question,
                        button_del_question, button_back, button_send,
                        callback.data.split('_')[0]
                    )
                )
                return
            if file_type == types.ContentType.DOCUMENT:
                await callback.message.answer_document(
                    document=file,
                    caption='<b>Вопрос от {date_question_format}</b>'
                            f'\n\n<b>Текст:</b>'
                            f'\n<code>{question_text}</code>',
                    reply_markup=keyboard_client_inline.send_question(
                        button_change_ask, button_change_time_question,
                        button_del_question, button_back, button_send,
                        callback.data.split('_')[0]
                    )
                )
                return
            if file_type == types.ContentType.VOICE:
                await callback.message.answer_voice(
                    voice=file,
                    caption='<b>Вопрос от {date_question_format}</b>'
                            f'\n\n<b>Текст:</b>'
                            f'\n<code>{question_text}</code>',
                    reply_markup=keyboard_client_inline.send_question(
                        button_change_ask, button_change_time_question,
                        button_del_question, button_back, button_send,
                        callback.data.split('_')[0]
                    )
                )
                return
    if file_type is None:
        await callback.message.edit_text(
            f'<b>Вопрос от {date_question_format}</b>'
            f'\n\n<b>Текст:</b>'
            f'\n<code>{question_text}</code>'
            f'\n\n<b>Время ответа:</b> {response_time}'
            f'\n<b>Статус:</b> не отправлен',
            reply_markup=keyboard_client_inline.send_question(
                button_change_ask, button_change_time_question,
                button_del_question, button_back, button_send,
                callback.data.split('_')[0]
            )
        )
        return
    try:
        await callback.message.edit_caption(
            caption='<b>Вопрос от {date_question_format}</b>'
                    f'\n\n<b>Текст:</b>'
                    f'\n<code>{question_text}</code>'
                    f'\n\n<b>Время ответа:</b> {response_time}'
                    f'\n<b>Статус:</b> не отправлен',
            reply_markup=keyboard_client_inline.send_question(
                button_change_ask, button_change_time_question,
                button_del_question, button_back, button_send,
                callback.data.split('_')[0]
            )
        )
        return
    except TelegramBadRequest:
        await callback.message.delete()
        if file_type == types.ContentType.PHOTO:
            await callback.message.answer_photo(
                photo=file,
                caption='<b>Вопрос от {date_question_format}</b>'
                        f'\n\n<b>Текст:</b>'
                        f'\n<code>{question_text}</code>'
                        f'\n\n<b>Время ответа:</b> {response_time}'
                        f'\n<b>Статус:</b> не отправлен',
                reply_markup=keyboard_client_inline.send_question(
                    button_change_ask, button_change_time_question,
                    button_del_question, button_back, button_send,
                    callback.data.split('_')[0]
                )
            )
            return
        if file_type == types.ContentType.VIDEO:
            await callback.message.answer_video(
                video=file,
                caption='<b>Вопрос от {date_question_format}</b>'
                        f'\n\n<b>Текст:</b>'
                        f'\n<code>{question_text}</code>'
                        f'\n\n<b>Время ответа:</b> {response_time}'
                        f'\n<b>Статус:</b> не отправлен',
                reply_markup=keyboard_client_inline.send_question(
                    button_change_ask, button_change_time_question,
                    button_del_question, button_back, button_send,
                    callback.data.split('_')[0]
                )
            )
            return
        if file_type == types.ContentType.DOCUMENT:
            await callback.message.answer_document(
                document=file,
                caption='<b>Вопрос от {date_question_format}</b>'
                        f'\n\n<b>Текст:</b>'
                        f'\n<code>{question_text}</code>'
                        f'\n\n<b>Время ответа:</b> {response_time}'
                        f'\n<b>Статус:</b> не отправлен',
                reply_markup=keyboard_client_inline.send_question(
                    button_change_ask, button_change_time_question,
                    button_del_question, button_back, button_send,
                    callback.data.split('_')[0]
                )
            )
            return
        await callback.message.answer_photo(
            photo=file,
            caption='<b>Вопрос от {date_question_format}</b>'
                    f'\n\n<b>Текст:</b>'
                    f'\n<code>{question_text}</code>'
                    f'\n\n<b>Время ответа:</b> {response_time}'
                    f'\n<b>Статус:</b> не отправлен',
            reply_markup=keyboard_client_inline.send_question(
                button_change_ask, button_change_time_question,
                button_del_question, button_back, button_send,
                callback.data.split('_')[0]
            )
        )
        return


@router_client.callback_query(FsmAskQuestion.send_question)
async def send_question(callback: CallbackQuery, state: FSMContext,
                        bot: Bot, session: AsyncSession):
    """
    Функция получения времени ответа на вопрос
    :param session: AsyncSession
    :param callback: CallbackQuery
    :param state: FSMContext
    :param bot: Bot
    :return:
    """
    context_data = await state.get_data()
    date_question = context_data.get('date_question')
    date_question_format = date_question.strftime("%d.%m.%Y в %H:%M")
    question = context_data.get('question')
    id_question = context_data.get('id_question')
    response_time = context_data.get('response_time')
    if callback.data == 'Мои вопросы':
        await state.clear()
        await get_ask_callback(callback, session)
        return
    if callback.data == 'Изменить текст вопроса':
        button_back = await base_components.get_button_by_id(session, 5)
        await state.update_data(mes_id=callback.message.message_id)
        file = context_data.get('file')
        if file is None:
            await callback.message.edit_text(
                f'<b>Вопрос от {date_question_format}</b>'
                f'\n\n<b>Текст:</b>'
                f'\n<code>{question}</code>'
                f'\n\n<b>Время ответа:</b> {response_time}'
                f'\n<b>Статус</b>: не отправлен'
                f'\n\n<b>---------------------------------</b>'
                f'\n<b>Пожалуйста, отправьте новый текст вопроса!</b>',
                reply_markup=keyboard_client_inline.back(button_back)
            )
            await state.set_state(FsmAskQuestion.question)
            return
        await callback.message.edit_caption(
            caption=f'<b>Вопрос от {date_question_format}</b>'
                    f'\n\n<b>Текст:</b>'
                    f'\n<code>{question}</code>'
                    f'\n\n<b>Время ответа:</b> {response_time}'
                    f'\n<b>Статус</b>: не отправлен'
                    f'\n\n<b>---------------------------------</b>'
                    f'\n<b>Пожалуйста, отправьте новый текст вопроса!</b>',
            reply_markup=keyboard_client_inline.back(button_back)
        )
        await state.set_state(FsmAskQuestion.question)
        return
    plans = await base_components.get_plans(session)
    if callback.data == 'Изменить время ответа':
        button_back = await base_components.get_button_by_id(session, 5)
        file = context_data.get('file')
        if file is None:
            await callback.message.edit_text(
                f'<b>Вопрос от {date_question_format}</b>'
                f'\n\n<b>Текст:</b>'
                f'\n<code>{question}</code>'
                f'\n\n<b>Время ответа:</b> {response_time}'
                f'\n<b>Статус</b>: не отправлен'
                f'\n\n<b>---------------------------------</b>'
                f'\n<b>Пожалуйста, выберите время ответа</b>',
                reply_markup=keyboard_client_inline.response_time_back(plans,
                                                                       response_time,
                                                                       button_back)
            )
            await state.set_state(FsmAskQuestion.response_time)
            return
        await callback.message.edit_caption(
            caption=f'<b>Вопрос от {date_question_format}</b>'
                    f'\n\n<b>Текст:</b>'
                    f'\n<code>{question}</code>'
                    f'\n\n<b>Время ответа:</b> {response_time}'
                    f'\n<b>Статус</b>: не отправлен'
                    f'\n\n<b>---------------------------------</b>'
                    f'\n<b>Пожалуйста, выберите время ответа</b>',
            reply_markup=keyboard_client_inline.response_time_back(plans,
                                                                   response_time,
                                                                   button_back)
        )
        await state.set_state(FsmAskQuestion.response_time)
        return
    button_change_ask = await base_components.get_button_by_id(session, 6)
    button_change_time_question = await base_components.get_button_by_id(session, 7)
    button_del_question = await base_components.get_button_by_id(session, 8)
    button_back = await base_components.get_button_by_id(session, 5)
    button_send = await base_components.get_button_by_id(session, 9)
    if callback.data == 'Восстановить вопрос':
        await state.update_data(status='active')
        await callback.answer(
            'Ваш вопрос был успешно восстановлен',
            show_alert=True
        )
        await bot.edit_message_reply_markup(
            chat_id=callback.from_user.id,
            message_id=callback.message.message_id,
            reply_markup=keyboard_client_inline.send_question(
                button_change_ask, button_change_time_question,
                button_del_question, button_back, button_send,
                callback.data.split('_')[0]
            )
        )
        return

    if callback.data == 'Удалить вопрос':
        button_restore_ask = await base_components.get_button_by_id(session, 10)
        button_show_menu = await base_components.get_button_by_id(session, 11)
        await state.update_data(status='delete')
        await callback.answer(
            'Ваш вопрос был успешно удален',
            show_alert=True
        )
        await bot.edit_message_reply_markup(
            chat_id=callback.from_user.id,
            message_id=callback.message.message_id,
            reply_markup=keyboard_client_inline.del_question(
                button_change_ask, button_change_time_question,
                button_restore_ask, button_show_menu
            )
        )
        return
    if callback.data == 'назад':
        await callback.message.edit_text(
            f'<b>Вопрос от {date_question_format}</b>'
            f'\n\n<b>Текст:</b>'
            f'\n<code>{question}</code>'
            f'\n\n<b>Время ответа:</b> {response_time}'
            f'\n<b>Статус</b>: не отправлен'
            f'\n\n<b>---------------------------------</b>'
            f'\n<b>Пожалуйста, выберите время ответа</b>',
            reply_markup=keyboard_client_inline.response_time_main_menu(
                plans, response_time, button_back)
        )
        await state.set_state(FsmAskQuestion.response_time)
        return
    if callback.data == 'Отправить':
        if response_time is None:
            mes_1 = await base_components.get_message_by_id(session, 6)
            await callback.answer(mes_1)
            return
        plan_id = context_data.get('plan_id')
        wallet = await base_wallet.get_wallet_by_user_id(session, callback.from_user.id)
        plan = await base_plan.get_plan_by_id(session, plan_id)
        if plan.price > wallet:
            mes_2 = await base_components.get_message_by_id(session, 8)
            await callback.answer(mes_2, show_alert=True)
            mes_3 = await base_components.get_message_by_id(session, 9)
            button_back = await base_components.get_button_by_id(session, 5)
            await callback.message.edit_text(
                mes_3,
                reply_markup=keyboard_client_inline.back_send(button_back)
            )
            return
        new_wallet = wallet - plan.price
        await base_wallet.update_wallet_user(session, callback.from_user.id,
                                             new_wallet)
        await base_question.update_update_question_status_send(session, id_question)
        mes = await base_components.get_message_by_id(session, 3)
        file_type = context_data.get('file_type')
        if file_type is None:
            await callback.message.edit_text(mes)
        else:
            await callback.message.delete()
            await callback.message.answer(mes)
        await command_start_callback(callback, state, session)
        chats = await base_chats.get_active_chat(session)
        for chat in chats:
            try:
                file = context_data.get('file')
                if file is None:
                    await bot.send_message(
                        chat_id=chat.url,
                        text=f'<b>Вопрос от {date_question_format}</b>'
                             f'\n\n<b>Текст:</b>'
                             f'\n<code>{question}</code>'
                             f'\n\n<b>Время ответа:</b> {response_time}',
                        reply_markup=keyboard_admin_inlain.ask(id_question)
                    )
                elif file_type == types.ContentType.PHOTO:
                    await bot.send_photo(
                        chat_id=chat.url,
                        photo=file,
                        caption=f'<b>Вопрос от {date_question_format}</b>'
                                f'\n\n<b>Текст:</b>'
                                f'\n<code>{question}</code>'
                                f'\n\n<b>Время ответа:</b> {response_time}',
                        reply_markup=keyboard_admin_inlain.ask(id_question)
                    )
                elif file_type == types.ContentType.VIDEO:
                    await bot.send_video(
                        chat_id=chat.url,
                        video=file,
                        caption=f'<b>Вопрос от {date_question_format}</b>'
                                f'\n\n<b>Текст:</b>'
                                f'\n<code>{question}</code>'
                                f'\n\n<b>Время ответа:</b> {response_time}',
                        reply_markup=keyboard_admin_inlain.ask(id_question)
                    )
                elif file_type == types.ContentType.VOICE:
                    await bot.send_voice(
                        chat_id=chat.url,
                        voice=file,
                        caption=f'<b>Вопрос от {date_question_format}</b>'
                                f'\n\n<b>Текст:</b>'
                                f'\n<code>{question}</code>'
                                f'\n\n<b>Время ответа:</b> {response_time}',
                        reply_markup=keyboard_admin_inlain.ask(id_question)
                    )
                elif file_type == types.ContentType.DOCUMENT:
                    await bot.send_document(
                        chat_id=chat.url,
                        document=file,
                        caption=f'<b>Вопрос от {date_question_format}</b>'
                                f'\n\n<b>Текст:</b>'
                                f'\n<code>{question}</code>'
                                f'\n\n<b>Время ответа:</b> {response_time}',
                        reply_markup=keyboard_admin_inlain.ask(id_question)
                    )
            except TelegramBadRequest:
                mes_4 = await base_components.get_message_by_id(session, 10)
                await callback.answer(
                    mes_4,
                    show_alert=True
                )
            except TelegramForbiddenError:
                mes_4 = await base_components.get_message_by_id(session, 10)
                await callback.answer(
                    mes_4,
                    show_alert=True
                )
        await state.clear()
