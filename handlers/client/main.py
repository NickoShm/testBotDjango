from aiogram import Router, F, Bot
from aiogram.types import Message, CallbackQuery
from aiogram.filters import CommandStart
from keyboards.client import keyboard_client_inline
from aiogram.fsm.context import FSMContext
from data_base import base_components, base_user, base_wallet, base_question
from sqlalchemy.ext.asyncio import AsyncSession
from keyboards.client import keyboard_client_reply

router_client = Router()


@router_client.message(CommandStart())
@router_client.message(F.text == 'ℹ️ Показать меню')
async def command_start(message: Message, state: FSMContext,
                        session: AsyncSession):
    """
    Функция ответа на команду старт
    :param session: AsyncSession
    :param state: FSMContext
    :param message: Message
    :return:
    """
    await state.clear()
    user = await base_user.add_new_user(session, message)
    mes = await base_components.get_message_by_id(session, 1)
    button_ask_question = await base_components.get_button_by_id(session, 1)
    button_my_ask = await base_components.get_button_by_id(session, 2)
    button_balance = await base_components.get_button_by_id(session, 3)
    button_support = await base_components.get_button_by_id(session, 4)
    my_ask = await base_question.get_count_question_user(session, message.from_user.id)
    wallet = await base_wallet.get_wallet_by_user_id(session, message.from_user.id)
    await message.answer(
        f'<b>{message.from_user.first_name} {message.from_user.last_name}</b>, '
        f'добро пожаловать!',
        reply_markup=keyboard_client_reply.board_main_menu()
    )
    await message.answer(
        mes,
        reply_markup=keyboard_client_inline.board_main_menu(
            button_ask_question, button_my_ask, button_balance,
            button_support, my_ask, wallet
        )
    )


async def command_start_callback(callback: CallbackQuery, state: FSMContext,
                                 session: AsyncSession):
    """
    Функция ответа на команду старт
    :param callback: CallbackQuery
    :param session: AsyncSession
    :param state: FSMContext
    :return:
    """
    await state.clear()
    await base_user.add_new_user_callback(session, callback)
    mes = await base_components.get_message_by_id(session, 1)
    button_ask_question = await base_components.get_button_by_id(session, 1)
    button_my_ask = await base_components.get_button_by_id(session, 2)
    button_balance = await base_components.get_button_by_id(session, 3)
    button_support = await base_components.get_button_by_id(session, 4)
    my_ask = await base_question.get_count_question_user(session, callback.from_user.id)
    wallet = await base_wallet.get_wallet_by_user_id(session, callback.from_user.id)
    await callback.message.answer(
        mes,
        reply_markup=keyboard_client_inline.board_main_menu(
            button_ask_question, button_my_ask, button_balance,
            button_support, my_ask, wallet
        )
    )
