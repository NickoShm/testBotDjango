from aiogram import Router, F, Bot, types
from aiogram.types import CallbackQuery
from keyboards.admin import keyboard_admin_inlain
from aiogram.fsm.context import FSMContext
from sqlalchemy.ext.asyncio import AsyncSession
from data_base import base_question, base_plan

router_admin = Router()


# router_admin.message.filter(IsAdmin())


@router_admin.callback_query(F.data == 'назад admin')
async def return_question(callback: CallbackQuery, state: FSMContext,
                          session: AsyncSession, bot: Bot):
    """
    Функция возврата вопроса
    :param bot: Bot
    :param callback: CallbackQuery
    :param session: AsyncSession
    :param state: FSMContext
    :return:
    """
    context_data = await state.get_data()
    id_question = context_data.get('id_question')
    mes_id = context_data.get('mes_id')
    question = await base_question.get_question_by_id(session, id_question)
    question_file_type = question.type_question
    date_question = question.created_at
    date_question_format = date_question.strftime("%d.%m.%Y в %H:%M")
    plan = await base_plan.get_plan_by_id(session, question.plan_id)
    if question_file_type is None:
        await bot.edit_message_text(
            chat_id=callback.message.chat.id,
            message_id=mes_id,
            text=f'<b>Вопрос от {date_question_format}</b>'
                 f'\n\n<b>Текст:</b>'
                 f'\n<code>{question.text_question}</code>'
                 f'\n\n<b>Время ответа:</b> {plan.name}',
            reply_markup=keyboard_admin_inlain.ask(id_question)
        )
        await state.clear()
        return
    await bot.edit_message_caption(
        chat_id=callback.message.chat.id,
        message_id=mes_id,
        caption=f'<b>Вопрос от {date_question_format}</b>'
                f'\n\n<b>Текст:</b>'
                f'\n<code>{question.text_question}</code>'
                f'\n\n<b>Время ответа:</b> {plan.name}',
        reply_markup=keyboard_admin_inlain.ask(id_question)
    )
    await state.clear()
