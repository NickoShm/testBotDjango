from aiogram.types import CallbackQuery, Message
from handlers.admin.main import router_admin
from aiogram import F, Bot, types
from keyboards.admin import keyboard_admin_inlain
from aiogram.fsm.context import FSMContext
from aiogram.fsm.state import State, StatesGroup
from datetime import datetime
from aiogram.filters import Command
from data_base import base_components, base_question, base_wallet, base_plan
from sqlalchemy.ext.asyncio import AsyncSession


@router_admin.message(F.text == 'Вопросы')
async def get_questions_funk(message: Message, session: AsyncSession):
    """
    Функция получения не отвеченных вопросов
    :param message: Message
    :param session: AsyncSession
    :return:
    """
    questions = await base_question.get_questions_active(session)
    if not questions:
        await message.answer(
            'Не отвеченных вопросов нет'
        )
    for question in questions:
        plan = await base_plan.get_plan_by_id(session, question.plan_id)
        question_type_file = question.type_question
        if question_type_file is None:
            await message.answer(
                f'<b>Вопрос от {question.created_at}</b>'
                f'\n\n<b>Текст:</b>'
                f'\n<code>{question.text_question}</code>'
                f'\n\n<b>Время ответа:</b> {plan.name}',
                reply_markup=keyboard_admin_inlain.ask(question.id_question)
            )
        else:
            if question_type_file == types.ContentType.PHOTO:
                await message.answer_photo(
                    photo=question.file_question,
                    caption=f'<b>Вопрос от {question.created_at}</b>'
                    f'\n\n<b>Текст:</b>'
                    f'\n<code>{question.text_question}</code>'
                    f'\n\n<b>Время ответа:</b> {plan.name}',
                    reply_markup=keyboard_admin_inlain.ask(question.id_question)
                )
            elif question_type_file == types.ContentType.VIDEO:
                await message.answer_video(
                    video=question.file_question,
                    caption=f'<b>Вопрос от {question.created_at}</b>'
                    f'\n\n<b>Текст:</b>'
                    f'\n<code>{question.text_question}</code>'
                    f'\n\n<b>Время ответа:</b> {plan.name}',
                    reply_markup=keyboard_admin_inlain.ask(question.id_question)
                )
            elif question_type_file == types.ContentType.DOCUMENT:
                await message.answer_document(
                    document=question.file_question,
                    caption=f'<b>Вопрос от {question.created_at}</b>'
                    f'\n\n<b>Текст:</b>'
                    f'\n<code>{question.text_question}</code>'
                    f'\n\n<b>Время ответа:</b> {plan.name}',
                    reply_markup=keyboard_admin_inlain.ask(question.id_question)
                )
            else:
                await message.answer_voice(
                    voice=question.file_question,
                    caption=f'<b>Вопрос от {question.created_at}</b>'
                            f'\n\n<b>Текст:</b>'
                            f'\n<code>{question.text_question}</code>'
                            f'\n\n<b>Время ответа:</b> {plan.name}',
                    reply_markup=keyboard_admin_inlain.ask(question.id_question)
                )
