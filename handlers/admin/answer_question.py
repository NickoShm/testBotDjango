from aiogram.types import CallbackQuery, Message
from handlers.admin.main import router_admin
from aiogram import F, Bot, types
from keyboards.admin import keyboard_admin_inlain
from aiogram.fsm.context import FSMContext
from aiogram.fsm.state import State, StatesGroup
from data_base import base_question
from sqlalchemy.ext.asyncio import AsyncSession
from aiogram.exceptions import TelegramBadRequest


class FsmAnswerAsk(StatesGroup):
    ask = State()


@router_admin.callback_query(F.data.startswith('ответить_'))
async def balance_callback(callback: CallbackQuery, session: AsyncSession,
                           state: FSMContext):
    """
    Функция ответа на вопрос
    :param state: FSMContext
    :param callback: callback
    :param session: AsyncSession
    :return:
    """
    await state.set_state(FsmAnswerAsk.ask)
    id_question = int(callback.data.split('_')[1])
    await state.update_data(id_question=id_question)
    question = await base_question.get_question_by_id(session, id_question)
    try:
        mes = await callback.message.edit_text(
            '<b>Напишите ответ на вопрос</b>'
            f'\n\n{question.text_question}',
            reply_markup=keyboard_admin_inlain.back()
        )
    except TelegramBadRequest:
        mes = await callback.message.edit_caption(
            caption='Напишите ответ на вопрос'
            f'\nn{question.text_question}',
            reply_markup=keyboard_admin_inlain.back()
        )
    await state.update_data(mes_id=mes.message_id)


@router_admin.message(FsmAnswerAsk.ask)
async def load_answer(message: Message, session: AsyncSession,
                      state: FSMContext, bot: Bot):
    """
    Функция загрузка ответа на вопрос
    :param session: AsyncSession
    :param state: FSMContext
    :param bot: Bot
    :param message: Message
    :return:
    """
    context_data = await state.get_data()
    id_question = context_data.get('id_question')
    mes_id = context_data.get('mes_id')
    question = await base_question.get_question_by_id(session, id_question)
    question_file = question.file_question
    if message.content_type == types.ContentType.TEXT:
        await bot.send_message(
            question.user_id,
            f'<b>Ваш вопрос:</b>'
            f'\n{question.text_question}'
            f'\n\n<b>Ответ:</b>'
            f'\n{message.text}'
        )
        await base_question.update_status_ask_and_ask(session, id_question,
                                                      message.text, None,
                                                      None)
    else:
        if message.content_type == types.ContentType.PHOTO:
            file = message.photo[-1].file_id
            type_file = message.content_type
            await bot.send_photo(
                question.user_id,
                photo=file,
                caption=f'<b>Ваш вопрос:</b>'
                f'\n{question.text_question}'
                f'\n\n<b>Ответ:</b>'
                f'\n{message.caption}'
            )
        elif message.content_type == types.ContentType.VIDEO:
            file = message.video.file_id
            type_file = message.content_type
            await bot.send_video(
                question.user_id,
                video=file,
                caption=f'<b>Ваш вопрос:</b>'
                f'\n{question.text_question}'
                f'\n\n<b>Ответ:</b>'
                f'\n{message.caption}'
            )
        elif message.content_type == types.ContentType.DOCUMENT:
            file = message.document.file_id
            type_file = message.content_type
            await bot.send_document(
                question.user_id,
                document=file,
                caption=f'<b>Ваш вопрос:</b>'
                f'\n{question.text_question}'
                f'\n\n<b>Ответ:</b>'
                f'\n{message.caption}'
            )
        else:
            file = message.voice.file_id
            type_file = message.content_type
            await bot.send_voice(
                question.user_id,
                voice=file,
                caption=f'<b>Ваш вопрос:</b>'
                        f'\n{question.text_question}'
                        f'\n\n<b>Ответ:</b>'
                        f'\n{message.caption}'
            )
        await base_question.update_status_ask_and_ask(session, id_question,
                                                      message.text, file,
                                                      type_file)
    if question_file is None:
        await bot.edit_message_text(
            chat_id=message.chat.id,
            message_id=mes_id,
            text='Ответ пользователю отправлен')
        return
    await bot.edit_message_caption(
        chat_id=message.chat.id,
        message_id=mes_id,
        caption='Ответ пользователю отправлен')
