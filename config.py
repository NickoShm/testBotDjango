from pydantic import BaseModel, Field, SecretStr
from pydantic_settings import BaseSettings as _BaseSettings
from pydantic_settings import SettingsConfigDict


class BaseSettings(_BaseSettings):
    model_config = SettingsConfigDict(extra="ignore", env_file=".env",
                                      env_file_encoding="utf-8")


class ConfigBot(BaseSettings, env_prefix="BOT_"):
    bot_token: SecretStr


class PostgresConfig(BaseSettings, env_prefix="POSTGRES_"):
    user: str
    password: SecretStr
    host: str
    port: int
    db: str


class PaymentConfig(BaseSettings, env_prefix="PAYMENT_"):
    api_key: SecretStr
    api_id: int
    secret_key: SecretStr
    shop: int


class AppConfig(BaseModel):
    config_bot: ConfigBot
    postgres: PostgresConfig
    payment: PaymentConfig


app_config = AppConfig(config_bot=ConfigBot(),
                       postgres=PostgresConfig(),
                       payment=PaymentConfig(),
                       )

ID_ADMIN = 5848337512
