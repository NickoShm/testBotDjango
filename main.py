import asyncio
# import logging
# import sys
from aiogram import Bot, Dispatcher
from aiogram.enums import ParseMode
from aiogram.client.default import DefaultBotProperties
from handlers.client.main import router_client
from handlers.admin.main import router_admin
# from config import app_config
from keyboards.button_menu import commands_list
from data_base import base_engine
from middlewares.middleware_db import DataBaseSession
import os


# bot = Bot(app_config.config_bot.bot_token.get_secret_value(),
#           default=DefaultBotProperties(parse_mode=ParseMode.HTML))
bot = Bot(os.getenv('BOT_TOKEN'),
          default=DefaultBotProperties(parse_mode=ParseMode.HTML))
dp = Dispatcher()
dp.include_router(router_client)
dp.include_router(router_admin)


async def on_startup():
    await bot.set_my_commands(commands=commands_list)
    # await base_engine.create_table()
    print('Бот запущен')


async def on_shutdown():
    print('Бот остановлен')


async def main() -> None:
    await bot.delete_webhook(drop_pending_updates=True)

    dp.update.middleware(DataBaseSession(base_engine.session_maker))

    dp.startup.register(on_startup)
    dp.shutdown.register(on_shutdown)

    await dp.start_polling(bot)


if __name__ == "__main__":
    # logging.basicConfig(level=logging.INFO, stream=sys.stdout)
    asyncio.run(main())
